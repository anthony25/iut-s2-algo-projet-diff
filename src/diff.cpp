/*
LICENCE : BSD

Copyright (c) 2013, RUHIER Anthony <aruhier@edu.univ-fcomte.fr>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/**
 * \file        diff.cpp
 * \author      Anthony RUHIER <anthony.ruhier@gmail.com>
 * \version     1.0
 * \date        08/05/2013
 * \brief       Algorithms project : diff-like program
 *
 * \details     Does what the UNIX diff command does : compares 2 files and
 *              print with '+' or '-' what has been added or removed.\n
 *
 * repo git : https://bitbucket.org/anthony25/iut-s2-algo-projet-diff
 * Group : S2A1
 */


#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <climits>


/**
 * \struct Line
 * \brief Line is an element of the linked list Text.
 */

struct Line {
	std::string cont;
	Line* next;
};
typedef Line* Text;


/**
 * \brief       Check if new() doesn't return NULL
 * \param       void* var : variable to test
 * \return      bool error : true if error
 */

bool checkNew(void* var)
{
    if(var == NULL)
    {
        std::cout << "Erreur durant l'allocation mémoire\n";
        return true;
    }

    else
        return false;
}


/**
 * \brief       Create a new element line to Text
 * \param       Text text : linked list to add the line
 * \param       std::string line : line to add at the end of text
 * \param       bool *error : true if case of error
 * \return      Text text : in case of void list, return an adresse pointing
 *              on the first element
 */

Text add2Text(Text text, std::string line, bool *error)
{
    Line* newLine = new Line;

    // Check if new() doesn't return NULL
    if((*error = checkNew((void*)newLine)))
        return text;

    newLine->cont.assign(line);
    newLine->next = NULL;

    if(text == NULL)
        text = newLine;

    else
    {
        Text tempText = text;

        while(tempText->next != NULL)
            tempText = tempText->next;

        tempText->next = newLine;
    }

    return text;
}


/**
 * \brief       Access to the Xth element
 * \param       Text text : linked list
 * \param       int nbElem : element position to access
 * \return      Text elem : element wanted, or NULL if nbElem < 1
 */

Text access2Elem(Text text, int nbElem)
{
    Text elem = text;

    if(nbElem < 1)
        elem = NULL;

    for(int i = 1; i < nbElem && elem != NULL; i++)
        elem = elem->next;

    return elem;
}


/**
 * \brief       Print the "Text" list content
 * \param       Text text : linked list where the text is stored
 * \param       int nbLines : (optionnal) read up to the specified line
 */

void printText(Text text, int nbLines = -1)
{
    // If nbLines not in parameters, print all the text
    if(nbLines == -1)
    {
        while(text != NULL)
        {
            std::cout << "  " << text->cont << std::endl;
            text = text->next;
        }
    }

    // Else, print until the line number sent in parameter
    else
    {
        for(int i = 0; i < nbLines && text != NULL; i++)
        {
            std::cout << "  " << text->cont << std::endl;
            text = text->next;
        }
    }
}


/**
 * \brief       Delete all lines in a text
 * \param       Text text : linked list to delete
 * \return      Text text = NULL : free linked list
 */

Text deleteText(Text text)
{
    Text tempText;

    while(text != NULL)
    {
        tempText = text->next;
        delete text;
        text = tempText;
    }

    return NULL;
}


/**
 * \brief       Delete double dimensions arrays
 * \param       short int **array : double dimension array
 * \param       int nbColumn : columns numbers
 * \return      int **result = NULL
 */

int **deleteDArray(int **array, int nbColumn)
{
    for(int i = 0; i < nbColumn; i++)
        delete [] array[i];

    delete [] array;

    return NULL;
}


/**
 * \brief       Check if the text has more than 0 line
 * \param       Text text : text to check
 * \param       int *nbLine : text line number
 * \return      Text text : if the text is empty, create a new empty line
 */

Text check0Line(Text text1, int *nbLine)
{
    if(nbLine < 0)
    {
        text1 = NULL;
        nbLine = 0;
    }

    return text1;
}


/**
 * \brief       Read file and store the content in the linked list "text"
 * \param       const std::string fileName : file name to read
 * \param       Text text : linked list where the file is stored
 * \param       int *length : elements number in the list "text"
 * \param       bool *error : check error
 * \return      Text text : linked list where the file is stored
 */

Text readFile(const char fileName[], Text text, int *length, bool *error)
{
    std::ifstream file(fileName);
    if(! file.is_open())
    {
        std::cout << "Lecture du fichier " << fileName << " impossible.\n";
        *error = true;
        return text;
    }

    std::string line;

    while((std::getline(file, line)) && (*length < INT_MAX - 1) && !(*error))
    {
        text = add2Text(text, line, error);
        (*length)++;
    }

    if(! file.eof() && !error)
    {
        /* Secutiry to avoid overflowing int
        The limit is INT_MAX - 1 because the LCS matrix properties are :
        [length_text1 + 1][length_text2 + 1] */
        if(*length == INT_MAX - 1)
        {
            std::cout << fileName << " trop grand, nombre de lignes max : ";
            std::cout << INT_MAX - 1<< ".\n";
        }

        else
            std::cout << "Erreur lors de la lecture de " << fileName << ".\n";

        *error = true;
    }

    file.close();

    // Set text to NULL, with a null length, if empty
    text = check0Line(text, length);

    return text;
}


/**
 * \brief       Reduce the compares operation if the 2 files beginning and/or
 *              end are the same
 * \details     The LCS is heavy in memory, and long to computes for big files.
 *              This fonction continues to read files until there is a
 *              difference between them, return the line where it
 *              stopped, and does the same for the end of the files.
 * \param       Text text1 : first file to compares
 * \param       Text text2 : Second text to compares
 * \param       int text1Length : elements number in the list "text1"
 * \param       int text2Length : elements number in the list "text2"
 * \param       bool* error : if true, error
 * \return      int* newProperties : array containing new proprieties for the
 *              LCS : {line number to start, line number in elem1, line
 *              number in elem2}
 */

int* LCSlength(Text text1, Text text2, int text1Length, int text2Length,
               bool* error)
{
    int start = 1, m = text1Length, n = text2Length;
    Line *elem1 = text1, *elem2 = text2;

    // Trim off the matching items at the beginning
    while((start <= m) && (start <= n) && elem1->cont.compare(elem2->cont)==0)
    {
        start++;
        elem1 = elem1->next;
        elem2 = elem2->next;
    }

    // Trim off the matching items at the end
    elem1 = access2Elem(text1, text1Length);
    elem2 = access2Elem(text2, text2Length);

    while((start <= m) && (start <= n) && elem1->cont.compare(elem2->cont)==0)
    {
        m--;
        n--;
        elem1 = access2Elem(text1, m);
        elem2 = access2Elem(text2, n);
    }

    int* newProperties = new int[3];

    // Check if new() doesn't return NULL
    if(!(*error = checkNew((void*)newProperties)))
    {
        newProperties[0] = start;
        // length - start + 1 because we start at line 1
        newProperties[1] = m - start + 1;
        newProperties[2] = n - start + 1;

        // Avoid keeping negatives line numbers
        newProperties[1] *= newProperties[1] > 0;
        newProperties[2] *= newProperties[2] > 0;
    }

    return newProperties;
}


/**
 * \brief       Computes the longest common subsequence matrix
 * \param       Text text1 : First text to compares
 * \param       Text text2 : Second text to compares
 * \param       int* properties : array containing {starting line, end line
 *              number in text1, end line number in text2}
 * \param       bool* error : if true, error
 * \return      int** lcs : matrix result
 */

int** computingLCS(Text text1, Text text2, int* properties, bool* error)
{
    const int text1Length = properties[1] + 1;
    const int text2Length = properties[2] + 1;
    int **lcs = new int*[text1Length];

    // Check if new() doesn't return NULL
    if((*error = checkNew((void*)lcs)))
        return lcs;

    // Initialize the first line and column
    for(int i = 0; i < text1Length; i++)
    {
        lcs[i] = new int[text2Length];

        // Check if new() doesn't return NULL
        if((*error = checkNew((void*)lcs)))
            return lcs;
        lcs[i][0] = 0;
    }

    for(int j = 0; j < text2Length; j++)
        lcs[0][j] = 0;

    Line *elem1, *elem2;
    elem1 = text1;

    for(int i = 1; i < text1Length; i++)
    {
        elem2 = text2;

        for(int j = 1; j < text2Length; j++)
        {
            if(elem1->cont.compare(elem2->cont) == 0)
                lcs[i][j] = lcs[i-1][j-1] + 1;

            else
                lcs[i][j] = std::max(lcs[i][j-1], lcs[i-1][j]);

            elem2 = elem2->next;
        }

        elem1 = elem1->next;
    }

    return lcs;
}


/**
 * \brief       Print diff
 * \param       int lcs[][] : longest common subsequence matrix
 * \param       Text text1 : First text to compares
 * \param       Text text2 : Second text to compares
 * \param       int i : elements number in the list "text1"
 * \param       int j : elements number in the list "text2"
 */

void printDiff(int** lcs, Text text1, Text text2, int i, int j)
{
    Line *elem1, *elem2;

    if(i > 0)
        elem1 = access2Elem(text1, i);

    if(j > 0)
        elem2 = access2Elem(text2, j);


    if((i > 0) && (j > 0) && elem1->cont.compare(elem2->cont) == 0)
    {
        printDiff(lcs, text1, text2, i - 1, j - 1);
        std::cout << "  " << elem1->cont << std::endl;
    }

    else if((j > 0) && ((i == 0) || (lcs[i][j-1] >= lcs[i-1][j])))
    {
        printDiff(lcs, text1, text2, i, j - 1);
        std::cout << "+ " << elem2->cont << std::endl;
    }

    else if((i > 0) && ((j == 0) || (lcs[i][j-1] < lcs[i-1][j])))
    {
        printDiff(lcs, text1, text2, i - 1, j);
        std::cout << "- " << elem1->cont << std::endl;
    }

    else
        std::cout << std::endl;

}


/**
 * \brief       Start the compare
 * \param       Text text1 : First text to compares
 * \param       Text text2 : Second text to compares
 * \param       int text1Length : elements number in the list "text1"
 * \param       int text2Length : elements number in the list "text2"
 * \param       bool* error : if true, error
 */

void compareLauncher(Text text1, Text  text2, int text1Length, int text2Length,
                     bool* error)
{
    int* LCSproperties = LCSlength(text1, text2, text1Length, text2Length,
                                   error);

    if(*error)
        exit(0);

    Text elem1LCS = access2Elem(text1, LCSproperties[0]);
    Text elem2LCS = access2Elem(text2, LCSproperties[0]);

    int** lcs = computingLCS(elem1LCS, elem2LCS, LCSproperties, error);

    printText(text1, LCSproperties[0] - 1);

    printDiff(lcs, elem1LCS, elem2LCS, LCSproperties[1], LCSproperties[2]);

    if(LCSproperties[1] < text1Length)
    {
        elem1LCS = access2Elem(text1, LCSproperties[1] + LCSproperties[0]);
        printText(elem1LCS);
    }

    lcs = deleteDArray(lcs, LCSproperties[1] + 1);
    delete [] LCSproperties;
    LCSproperties = NULL;
    elem1LCS = NULL;
    elem2LCS = NULL;
}


int main(int argc, char *argv[])
{
    if(argc != 3)
        std::cout << "Erreur, help : diff file1 file2\n";

    else
    {
        Text text1 = NULL, text2 = NULL;
        int text1Length = 0, text2Length = 0;
        bool error = false;

        text1 = readFile(argv[1], text1, &text1Length, &error);

        if(! error)
            text2 = readFile(argv[2], text2, &text2Length, &error);

        if(! error)
            compareLauncher(text1, text2, text1Length, text2Length, &error);

        text1 = deleteText(text1);
        text2 = deleteText(text2);
    }

    return 0;
}

